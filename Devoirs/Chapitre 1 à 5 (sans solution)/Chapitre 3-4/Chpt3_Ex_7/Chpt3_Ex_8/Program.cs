﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chpt3_Ex_8
{
    public int Add(params int[] values)
    {
        int somme = 0;
        foreach (int value in values)
        {
            somme += valuesa;
        }
        return somme;
    }

    class Program
    {
      
        public static void Main(string[] args)
        {
            int add1 = Add(1);
            int add2 = Add(2, 2, 2, 2);
            int[] addarray = new int[3] { 3, 3, 3 };
            int add3 = Add(addarray);
            Console.ReadLine();
        }
    }
}
