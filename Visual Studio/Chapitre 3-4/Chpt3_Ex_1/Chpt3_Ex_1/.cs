﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chpt3_Ex_1
{
    class Program
    {
        static public int GetMax(int n1, int n2)
        {
                if (n1 > n2)
            {
                return n1;
            }
                else
            {
                return n2;
            }
                
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Entrer le premier nombre : ");
            int n1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Entrer le deuxième nombre : ");
            int n2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Entrer le troisième nombre : ");
            int n3 = int.Parse(Console.ReadLine());

            int nombrePlusGrand1 = GetMax(n1, n2);
            int nombrePlusGrand2 = GetMax(nombrePlusGrand1, n3);

            System.Console.WriteLine("Le plus gros nombre est : {0}", nombrePlusGrand2);
            Console.ReadLine();
        }
    }
}
