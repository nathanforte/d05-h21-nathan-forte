﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chpt3_Ex_7
{
    class Calculator
    {
        public void Double(int nb, out int doubleChiffre)
        {
            nb *= 2;
            doubleChiffre = nb;
        }
    }
    class Program
    {

        static void Main(string[] args)
        {
            int doubleChiffre;
            Console.WriteLine("Entrer un nombre: " );
            int nb = int.Parse(Console.ReadLine());
            new Calculator().Double(nb, out doubleChiffre);
            Console.WriteLine(doubleChiffre);
            Console.ReadLine();
        }
    }
}
