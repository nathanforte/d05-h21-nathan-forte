﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_Switch
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Entrer la note: ");
            int note = int.Parse(Console.ReadLine());
            switch (note)
            {
                case 1:
                    note *= 10;
                    break;
                case 2:
                    note *= 10;
                    break;
                case 3:
                    note *= 10;
                    break;
                case 4:
                    note *= 100;
                    break;
                case 5:
                    note *= 100;
                    break;
                case 6:
                    note *= 100;
                    break;
                case 7:
                    note *= 1000;
                    break;
                case 8:
                    note *= 1000;
                    break;
                case 9:
                    note *= 1000;
                    break;
                default:
                    Console.WriteLine("Entrer une note valide (1 à 9)");
                    break;
            }
        }
    }
}
